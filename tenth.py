class First:
    def method1(self):
        print "This is class 1"

class Second(First):
    def method1(self):
        print "This is class 2"

class Three(First):
    def method1(self):
        print "This is class 3"

class Four(Second,Three):
    def method1(self):
        print "This is class 4"

fir=First()
sec=Second()
thi=Three()
four=Four()

fir.method1()
sec.method1()
thi.method1()
four.method1()